function RunSimulations()
% Run simulations required for figures and tables included in the manuscript.

if exist('outputs', 'dir')==0
    mkdir('outputs');
end

% Generate 'in_house_simulation_results.mat' used in Figure 3
run.ApoptoCellSimulationsInHouseCohort();

% Generate 'smac_mimetics_in_silico_clinical_trial.mat' used in Figure 4
run.SmacMimeticsInSilicoClinicalTrial();
end
