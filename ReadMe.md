# System-based Approaches as Prognostic Tools for Glioblastoma  
---------------------------------------------------------

Source code and datasets to reproduce the computational analyses presented
in the review *_System-based Approaches as Prognostic Tools for Glioblastoma_*
submitted at *BMC Cancer* by Manuela Salvucci\*, Zaitun Zakaria\*, Steven Carberry, Amanda 
Tivnan, Volker Seifert, Donat Kögel, Brona M. Murphy, and Jochen H.M. Prehn.

\* Co-authorship.

### Requirements

The following software and toolboxes are required:

* MATLAB (release 2014b) with:
	*  Parallel Computing Toolbox;
	*  Statistics Toolbox;
* R (release 3.4.3) with:
    *  "optparse" package (1.4.4);
    *  "data.table" package (1.10.4-3);
	*  "survival" package (2.41-3);
	*  "car" package (2.1-5);

R binary Rscript must be in PATH.


### Directory structure

```
.
├── ReadMe.md
├── RunSimulations.m
├── MakeFiguresAndTables.m
├── data/
│   ├── in_house_apoptocell_proteins_data.csv
│   ├── in_house_apoptocell_proteins_data.md
│   ├── in_house_clinical_data.csv
│   └── in_house_clinical_data.md
├── +get/
│   ├── InHouseApoptoCellSimulationInputs.m
│   └── InHouseClinicalData.m
├── +run/
│   ├── ApoptoCellSimulationsInHouseCohort.m
│   └── SmacMimeticsInSilicoClinicalTrial.m
├── +make/
│   ├── ApoptoCellPredictionsFigure.m
│   ├── ClinicalCharacteristicsTable.m
│   ├── CoxRegressionModelsTable.m
│   ├── ProteinExpressionBoxplotFigure.m
│   ├── ProteinExpressionKaplanMeierFigure.m
│   └── SmacMimeticsInSilicoClinicalTrialFigure.m
├── +cohort_utilities/
│   ├── +alignment/
│   │   ├── AbstractAligner.m
│   │   └── MedianAlignment.m
│   ├── ApoptoCell.m
│   ├── DummyAggregator.m
│   ├── GetDefaultApoptoCellKineticParameters.m
│   ├── MakeKaplanMeierPlot.m
│   ├── ReadSimulationResults.m
│   └── SimulateCohort.m
└── +matlab_utilities/
    ├── +mosaic_plot/
    │   ├── license.txt
    │   ├── mosaic_plot.m
    │   ├── multi_text.m
    │   ├── plot_rectangles.m
    │   └── source.md
    ├── +panel/
    │   ├── license.txt
    │   ├── panel.m
    │   └── source.md
    ├── +plotSpread/
    │   ├── isEven.m
    │   ├── license.txt
    │   ├── myErrorbar.m
    │   ├── plotSpread.m
    │   ├── repeatEntries.m
    │   └── source.md
    ├── R_helpers/
    │   ├── ComputeMedianSurvival.R
    │   ├── RunMultipleCoxModels.R
    │   └── RunMultipleLogranks.R
    ├── CombineLegends.m
    ├── ComputeMedianSurvivalR.m
    ├── ConcatenateTables.m
    ├── FormatPValue.m
    ├── iif.m
    ├── MakeCombinedBoxplotSpreadPlot.m
    ├── MaximizeFigure.m
    ├── patchline_license.txt
    ├── patchline.m
    ├── patchline_source.md
    ├── ProgressBar.m
    ├── RunMultipleCoxModelsR.m
    ├── RunMultipleLogranksR.m
    ├── TemporarilyDisableWarnings.m
    ├── ticklabelformat_license.txt
    ├── ticklabelformat.m
    └── ticklabelformat_source.md
```

The +matlab_utilities package also includes third-party software downloaded
from [MATLAB file exchange](https://uk.mathworks.com/matlabcentral/fileexchange/). 
See corresponding source.md and license.txt files for details.

More information can be found in:

* doc string for each .m and .R file;
* code book for each .csv file.


### Usage

Run:

1. `RunSimulations()` to call functions to generate all the simulation results:
    *  `in_house_simulation_results.mat`;
    *  `smac_mimetics_in_silico_clinical_trial.mat`;
   and save them in a folder named `outputs`. For convenience, precomputed
   simulation results are provided in a folder named `cached_outputs`;
2. `MakeFiguresAndTables()` to generate figures and tables from the paper in
   a folder named `figures_and_tables`:   
    *  Table 2 (clinical_characteristics_table.csv);
    *  Table 3 (clinical_characteristics_table.csv);
    *  Figure 1 (protein_expression_boxplot_figure.pdf);
    *  Figure 2 (protein_expression_kaplan_meier_figure.pdf);
    *  Figure 3 (apoptocell_predictions_figure.pdf);
    *  Figure 4 (in_silico_smac_mimetics_trial_figure.pdf);


## Citation

When using these datasets and code, please cite https://doi.org/10.5281/zenodo.3473419
and the corresponding review paper.


## Contact us:

* Prof. Jochen H. M. Prehn (JPrehn@rcsi.ie);
* Manuela Salvucci (manuelasalvucci@rcsi.ie).


## Funding

This research was supported by grants from Brain Tumor Ireland, Science Foundation
Ireland (13/IA/1881 and 14/IA/2582), the European Union’s Seventh Framework Programme
for research, technological development, and demonstration under grant agreement
306021 (APO-DECIDE) to JHMP and an International Training Network supported by the
European Union’s H2020 Programme for research, under grant agreement 766069 (GLIOTRAIN)
to JHMP and BMM.

----------------------
