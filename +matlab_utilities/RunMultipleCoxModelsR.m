function [full_model_stats, per_cox_term_stats, per_cox_term_level_stats] =...
    RunMultipleCoxModelsR(sample_set, survival, has_event, cox_terms)
% Runs multiple cox models, one per sample_set
% Input arguments:
% - sample_set: categorical, identifier for each cox model to run;
% - survival: vector, time;
% - has_event: logical, true if event has occured, false if event hasn't occurred;
% - cox_terms: table with terms to include in cox model
% Output:
% - full_model_stats: table, one row per cox model/sample_set with columns:
%   sample_set, n_samples, n_events, logrank_p, lrt_p and c_index;
% - per_cox_term_stats: table, one row per cox term with
%   columns: sample_set, cox_term, is_categorical, ref_level, lrt_p;
% - per_cox_term_level_stats: table, one row per cox term level with
%   columns: sample_set, cox_term, cox_term_level, coef, se, hr, low_ci, high_ci;

    function t = GetFactorLevels(cox_term_name)
        cats = categories(cox_terms.(cox_term_name));
        t = table(repmat({cox_term_name}, numel(cats),1), cats,...
            'VariableNames', {'cox_term', 'cox_term_level'});
    end

t = [table(sample_set, survival, has_event), cox_terms];
cox_terms_as_factor = cox_terms.Properties.VariableNames(...
    varfun(@iscategorical, cox_terms, 'OutputFormat', 'uniform'));
if numel(cox_terms_as_factor) > 0
    cox_term_factors = cellfun(@GetFactorLevels, cox_terms_as_factor,...
        'UniformOutput', false);
    cox_term_factors = vertcat(cox_term_factors{:});
else
    cox_term_factors = table({}, {},...
        'VariableNames', {'cox_term', 'cox_term_level'});
end
input_filename = [tempname(), '.csv'];
cox_term_factors_filename = [tempname(), '.csv'];
full_model_stats_filename = [tempname(), '.csv'];
per_cox_term_stats_filename = [tempname(), '.csv'];
per_cox_term_level_stats_filename = [tempname(), '.csv'];
writetable(t, input_filename);
writetable(cox_term_factors, cox_term_factors_filename);
R_script_name = fullfile(fileparts(mfilename('fullpath')), 'R_helpers', 'RunMultipleCoxModels.R');
% Rscript must be in PATH (enviromental variable set)
cmd_line = sprintf(...
    'Rscript %s --input_filename %s --cox_term_factors_filename %s --full_model_stats_filename %s --per_cox_term_stats_filename %s --per_cox_term_level_stats_filename %s',...
    R_script_name,input_filename, cox_term_factors_filename,...
    full_model_stats_filename, per_cox_term_stats_filename,...
    per_cox_term_level_stats_filename);
[status, output] = system(cmd_line);
if status == 0
    full_model_stats = readtable(full_model_stats_filename);
    per_cox_term_stats = readtable(per_cox_term_stats_filename);
    per_cox_term_stats.is_categorical = strcmp(per_cox_term_stats.is_categorical, 'TRUE');
    per_cox_term_level_stats = readtable(per_cox_term_level_stats_filename);
    if numel(output)
        warning('RunMultipleCoxModelsR:RWarning', '%s', output);
    end
else
    error('error occured while running RunMultipleCoxModels.R: %s', output);
end
delete(input_filename);
delete(cox_term_factors_filename);
delete(full_model_stats_filename);
delete(per_cox_term_stats_filename);
delete(per_cox_term_level_stats_filename);
end
