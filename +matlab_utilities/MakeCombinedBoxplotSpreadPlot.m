function [sh, bh] = MakeCombinedBoxplotSpreadPlot(data, G, varargin)
% Make boxplot combined with dots for individual data-points.
% Input arguments:
% - data: column vector, input data;
% - G: cell array, grouping data with one column per level of grouping;
% - key/value pair:
%   - labels_rotation: integer, angle of rotation for labels;
%   - factorseparator: factor separator as in MATLAB boxplot function;
%   - colors: character array, color;
%   - marker_size: scalar, marker size ;
%   - marker_type: char, marker type;
%   - offset: numeric, offset between boxplot and corresponding spread plot.
%     If 0, the spread plot and the boxplot are aligned;
%   - density_color_map: colormap (n*3 array with RGB values), used to color
%     dots based on distribution density.
% Outputs:
% - sh: 3-by-1 cell array with handles to distributions, mean/median etc,
%   and the axes, respectively;
% - bh: handle to boxplot.

p = inputParser();
p.addOptional('labels_rotation', 0); 
p.addOptional('factorseparator', []); 
p.addOptional('colors', 'k');
p.addOptional('marker_size', 12);
p.addOptional('marker_type', 'o'); 
p.addOptional('offset', 0);
p.addOptional('density_color_map', []);
p.parse(varargin{:});

[G, labels] = ProcessGroups(G);
distribution_colors = arrayfun(@(data) data, p.Results.colors, 'UniformOutput', false);
sh = plotColoredSpread(data, G, p, distribution_colors);

if p.Results.offset == 0
    widths = 0.75;
else
    widths = 0.1;
end

bh = boxplot(data, G,...
    'factorseparator', p.Results.factorseparator,...
    'symbol', '', 'widths', widths, 'colors', p.Results.colors);
set(bh, 'linewidth', 0.5);
set(findobj(bh, 'tag', 'Median'), 'linewidth', 1, {'color'}, distribution_colors');
set(findobj(gcf, '-regexp', 'Tag', '\w*Whisker'), 'LineStyle', '-');

if p.Results.offset ~= 0
    set(gca,...
        'XLim', [1 - p.Results.offset, numel(unique(G)) + 2*p.Results.offset],...
        'XTick', get(gca, 'XTick') + p.Results.offset/2);
end

set(gca, 'XTickLabel', labels, 'XTickLabelRotation', p.Results.labels_rotation);
end

function handles = plotColoredSpread(data, G, p, distribution_colors)
% Call plotSpread and set correct colors.
% Input arguments:
% - data: as defined in the top level function;
% - G: as defined in the top level function;
% - p: input parser object;
% - distribution_colors: cell array with colors as used by plotSpread; 
% Output:
% - handles: cell array with handles as returned by plotSpread.

handles = matlab_utilities.plotSpread.plotSpread(data,...
    'distributionIdx', G + p.Results.offset,...
    'distributioncolors', distribution_colors,...
    'distributionmarkers', '.', 'spreadwidth', 0.5);
handles_dist = handles{1};
set(handles_dist(~isnan(handles_dist)), 'Marker', p.Results.marker_type,...
    'MarkerSize', p.Results.marker_size,...
    {'MarkerEdgeColor'}, distribution_colors');

if ~isempty(p.Results.density_color_map)
    cmap = p.Results.density_color_map;
    for i = 1:max(G)
        if ~isnan(handles_dist(i))
            values = data(G == i);
            pd = fitdist(values, 'Kernel', 'Kernel', 'normal');
            densities = pdf(pd, values);
            colors = cmap(round(densities ./ max(densities) .* size(cmap, 1)), :);
            data = get(handles_dist(i), 'XData');
            hold on;
            hs = scatter(data, values, p.Results.marker_size, colors,...
                'filled', 'MarkerEdgeColor', 'k');
            set(hs, 'Marker', p.Results.marker_type);
        end
    end
    delete(handles_dist);
end

end

function [new_G, labels] = ProcessGroups(G)
% grp2idx-like function for multi-level groups.
% Input argument:
% - G: as defined in the top level function;
% Outputs:
% - new_G: integer identifier for each unique group;
% - labels: label identifier for each unique group.

t = table(G{:});
stats = grpstats(t, t.Properties.VariableNames, [], 'DataVars', false);
stats.idx = (1:height(stats))';
G = join(t, stats);
new_G = G.idx;
labels = rowfun(@(s) strjoin(cellstr(s), '\n'), stats,...
    'InputVariables', t.Properties.VariableNames, 'SeparateInputs', false,...
    'OutputFormat', 'cell');
end
