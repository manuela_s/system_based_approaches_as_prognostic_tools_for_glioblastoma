function MaximizeFigure()
% Maximize figure.

set(gcf, 'Position', get(0, 'Screensize'));
end
