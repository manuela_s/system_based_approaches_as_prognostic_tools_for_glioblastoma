classdef ProgressBar < handle
    % Progress bar.
    % It uses waitbar if a GUI is available, otherwise prints
    % status updates to the display every 10s.
    % To use:
    % pb = ProgressBar(tot_work_items, name)
    % for i = 1:tot_work_items
    %  ...
    %   pb.update()
    % end
    
    properties
        tot_work_items; % Total number of work items;
        work_items_done = 0; % Number of work items completed so far;
        work_items_done_by_workers = []; % Array with number of work items completed on remote workers;
        h_waitbar; % Handle to the underlying waitbar object if GUI is available;
        start_time; % Time when work started as returned by now();
        last_update = 0; % Timestamp for last update printed;
        last_progress = 0; % The last progress that was reported;
        name; % String for ProgressBar;
        tmp_dir; % Directory to keep progress data from workers;
    end
    
    methods
        
        function obj = ProgressBar(tot_work_items, name, varargin)
            % Constructor.
            % Input arguments:
            % - tot_work_items: integer, total number of work items;
            % - name: char array, label for ProgressBar.
            
            if nargin > 1
                obj.name = name;
            end
            
            p = inputParser();
            p.addParameter('enable_gui', true, @islogical);
            p.parse(varargin{:});

            obj.tmp_dir = tempname();
            mkdir(obj.tmp_dir);
            obj.tot_work_items = tot_work_items;
            obj.start_time = now();
            if usejava('awt') && p.Results.enable_gui
                obj.h_waitbar = waitbar(0);
                obj.h_waitbar.CurrentAxes.Title.Interpreter = 'none';
            end
            obj.SetProgress(0);
        end
        
        function update(obj, inc)
            % Register additional work completed.
            % Input argument:
            % - inc: optional number of new work items completed. Defaults
            %   to 1.
            
            if nargin < 2
                inc = 1;
            end
            obj.SetProgress(obj.work_items_done + inc);
        end
        
        function SetProgress(obj, work_items_done)
            % Set the number of work items completed so far.
            % Input argument:
            % - work_items_done: number of work items completed so far.
            
            obj.work_items_done = work_items_done;
            % Display progress if more than 10 seconds have passed or all
            % simulations are finished.
            if (now() > (obj.last_update + 10/86400) || work_items_done == obj.tot_work_items)
                task = getCurrentTask();
                obj.SaveWorkerProgress(task);
                if isempty(task) || task.ID == 1
                    % When running in a worker pool, only task 1 does
                    % progress reports.
                    obj.DisplayProgress();
                end
                obj.last_update = now();
            end
        end
        
        function DisplayProgress(obj)
            % Update ProgressBar or print progress to console.
            
            total_progress = obj.GetWorkerProgress();
            if (total_progress == obj.tot_work_items &&...
                total_progress == obj.last_progress)
                % Have already reported 100% progress: skipping
                return
            end
            if total_progress == obj.tot_work_items
                details = sprintf('%s: Total: %s', obj.name,...
                    datestr(now() - obj.start_time, 'dd-HH:MM:SS'));
            elseif total_progress > 0
                details = sprintf('%s: ETA %s', obj.name, datestr(...
                    obj.start_time + obj.tot_work_items *...
                    (now() - obj.start_time())/total_progress));
            else
                details = obj.name;
            end
            update_str = sprintf('%s: %d/%d. %s',...
                datestr(now()),...
                total_progress, obj.tot_work_items,...
                details);
            
            if usejava('awt') && any(ishandle(obj.h_waitbar))
                waitbar(total_progress/obj.tot_work_items, obj.h_waitbar, details);
            else
                disp(update_str);
            end
            
            % Report the final status once to the console even if GUI is
            % available
            if usejava('awt') && any(ishandle(obj.h_waitbar)) &&...
                    total_progress == obj.tot_work_items
                disp(update_str)
            end
            
            obj.last_progress = total_progress;
        end
        
        function delete(obj)
            % Destructor.
            task = getCurrentTask();
            obj.SaveWorkerProgress(task);
            if isempty(task) % Pool workers should not do cleanup.
                obj.DisplayProgress();
                delete(fullfile(obj.tmp_dir, '*.mat'));
                rmdir(obj.tmp_dir);
                if usejava('awt') && any(ishandle(obj.h_waitbar))
                    close(obj.h_waitbar);
                end
            end
        end
        
        function SaveWorkerProgress(obj, task)
            % Write progress from a worker to a file.
            % When running with multiple workers (parfor), each worker
            % writes progress to a file, and the first worker prints
            % updated progress.
            % Input argument:
            % - task: optional argument with the parallel.task object
            %   representing the current task. Leave unset if not run from
            %   a worker thread.
            
            if isempty(task)
                task_id = 0;
            else
                task_id = task.ID;
            end
            file_name = fullfile(obj.tmp_dir, sprintf('%d.mat', task_id));
            work_items_done = obj.work_items_done; %#ok<NASGU>
            save(file_name, 'work_items_done');
        end
        
        function total_progress = GetWorkerProgress(obj)
            % Read progress from all workers.
            
            files = dir(fullfile(obj.tmp_dir, '*.mat'));
            for i = 1:numel(files)
                try
                    % There is a race-condition between reading and writing
                    % the files. Ignore errors.
                    s = load(fullfile(obj.tmp_dir, files(i).name));
                    obj.work_items_done_by_workers(i) = s.work_items_done;
                end
            end
            total_progress = sum(obj.work_items_done_by_workers);
        end
    end
end
