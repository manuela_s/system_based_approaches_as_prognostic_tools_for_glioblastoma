function wstate = TemporarilyDisableWarnings(varargin)
% Temporarily disable warnings.
% Takes one or more warning message ids as input arguments (varargin)
% Returns a onCleanup object, which will restore warnings to the original
% state when cleared.

old_state = warning('query', 'all');
for i = 1:nargin
    warning('off', varargin{i});
end
wstate = onCleanup(@() warning(old_state));
end
