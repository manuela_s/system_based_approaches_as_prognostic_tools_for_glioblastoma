function ProteinExpressionKaplanMeierFigure()
% Make figure 2 panels A-E.
% Plot Kaplan-Meier plots using median expression as cut-off for each of the
% 5 proteins input for APOPTO-CELL.

% Get clinical data and APOPTO-CELL protein expression data
cli = get.InHouseClinicalData();
proteins = get.InHouseApoptoCellSimulationInputs();
protein_colnames = {'apaf1', 'caspase3', 'caspase9', 'smac', 'xiap'};
protein_names = {'Apaf-1', 'Procaspase-3', 'Procaspase-9', 'SMAC', 'XIAP'};

data = innerjoin(cli, proteins,...
    'keys', {'cohort', 'sub_cohort', 'universal_patient_id', 'samples', 'tumour_samples'},...
    'leftvariables', {'universal_patient_id',...
    'progression_free_survival_months','is_censored_progression_free_survival'},...
    'rightvariables', protein_colnames);

% Make figure
figure();
p = matlab_utilities.panel.panel();
% Create a 2x5 grid to plot in:
% - one row for Kaplan-Meier plots and one row for corresponding legends;
% - one column for each protein.
p.pack({[], {5}}, numel(protein_colnames));

for i = 1:numel(protein_colnames)
    % Dichotomize patients by median protein concentration
    protein_groups = categorical(...
        data.(protein_colnames{i}) >median(data.(protein_colnames{i})),...
        [true, false],...
        {'>median', '<=median'});
    
    p(1,i).select();
    cohort_utilities.MakeKaplanMeierPlot(...
        data.progression_free_survival_months,...
        data.is_censored_progression_free_survival=='yes',...
        'groups', protein_groups,...
        'colors', {[0, 0, 0]; [0.5, 0.5, 0.5]},...
        'xtick', 0:12:60,...
        'xlim', [0, 60],...
        'xlabel', '',...
        'ylabel', '',...
        'title', protein_names{i},...
        'stats_fontsize', 8,...
        'stats_position', 'top_right',...
        'censoredmarkersize', 2,...
        'kmlinewidth', 1);
    
    % Move legend to separate panel
    p(2,i).select(legend())
end

hx = xlabel(p(1), 'Time [months]');
hy = ylabel(p(1), 'Progression-free survival');
set([hx, hy], 'color', 'k');

p.descendants.marginleft = 10;
p.descendants.marginright = 10;
p.descendants.marginbottom = 15;
p.marginbottom = 5;

p.fontname = 'arial';
p.fontsize = 8;

p.export(...
    fullfile('figures_and_tables','protein_expression_kaplan_meier_figure.pdf'),...
    '-rx',...
    '-h54',...
    '-w180');
end
