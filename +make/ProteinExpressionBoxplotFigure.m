function ProteinExpressionBoxplotFigure()
% Make figure 1 panels B-F.
% Plot protein expression measured by WB for each of the 5 proteins input
% for APOPTO-CELL.

% Get protein expressions
proteins = get.InHouseApoptoCellSimulationInputs();
proteins.tumour_samples = renamecats(proteins.tumour_samples,...
    {'newly_diagnosed', 'recurrent'},...
    {'newly-diagnosed', 'recurrent'});

% Add number of patients per tumour_samples
stats = grpstats(proteins, 'tumour_samples', [], 'DataVars', false);
stats.pretty_tumour_samples = cellfun(...
    @(str, n) sprintf('%s (n=%d)', str, n),...
    cellstr(stats.tumour_samples),...
    num2cell(stats.GroupCount),...
    'UniformOutput', false);
proteins = innerjoin(proteins, stats, 'key', 'tumour_samples');

protein_names = {'apaf1', 'caspase3', 'caspase9', 'smac', 'xiap'};
concs = stack(proteins, protein_names,...
    'NewDataVariable', 'abs_conc',...
    'IndexVariableName', 'protein');

% Make figure
figure();
matlab_utilities.MaximizeFigure();
p = matlab_utilities.panel.panel();
p.pack('h', numel(protein_names));

MakeFigure1Helper(p(1), concs, 'apaf1', 'Apaf-1', '%.2f', [-0.01, 0.56]);
MakeFigure1Helper(p(2), concs, 'caspase3', 'Procaspase-3', '%.2f', [-0.075, 3.51]);
MakeFigure1Helper(p(3), concs, 'caspase9', 'Procaspase-9', '%.3f', [-0.0005, 0.025]);
MakeFigure1Helper(p(4), concs, 'smac', 'SMAC', '%.2f', [0, 0.56]);
MakeFigure1Helper(p(5), concs, 'xiap', 'XIAP', '%.2f', [0, 0.141]);

hy = ylabel(p, 'Estimated concs. [\muM]');
set(hy, 'color', 'k');

p.descendants.marginleft = 10;
p.descendants.marginright = 10;
p.marginbottom = 35;

p.fontname = 'arial';
p.fontsize = 8;

p.export(...
    fullfile('figures_and_tables','protein_expression_boxplot_figure.pdf'),...
    '-rx',...
    '-h64',...
    '-w180');
end

function MakeFigure1Helper(p, concs, protein, pretty_protein, ytick_format, ylims)
% Make a boxplot and dot plot for one protein broken down by tumour_samples.
% Input arguments:
% - p: instance of panel class, panel to create plot in;
% - concs: table, one row per patient/protein with columns:
%   - abs_conc: double, protein concentration in uM;
%   - protein: categorical, protein name;
% - protein: string, name of protein to plot. Needs to match protein column
%   in the concs table;
% - pretty_protein: string, name of protein, formatted to use in title;
% - ytick_format: string, sprintf format for formatting yticks;
% - ylims: vector, 2-elements vector with y limits.

% Subset table by protein to plot
sub_concs = concs(concs.protein==protein, :);

p.select();
matlab_utilities.MakeCombinedBoxplotSpreadPlot(...
    sub_concs.abs_conc,...
    {sub_concs.pretty_tumour_samples},...
    'colors', 'k',...
    'offset', 0.3,...
    'marker_type', '.',...
    'marker_size', 4);
matlab_utilities.ticklabelformat(gca, 'y', ytick_format);
title(p, pretty_protein);

% Calculates Mann-Whitney U-test pvalue
pvalue = ranksum(...
    sub_concs.abs_conc(sub_concs.tumour_samples=='newly-diagnosed'),...
    sub_concs.abs_conc(sub_concs.tumour_samples=='recurrent'));
text(1, 1, sprintf('%s ', matlab_utilities.FormatPValue(pvalue)),...
    'units', 'normalized',...
    'HorizontalAlignment', 'right',...
    'VerticalAlignment', 'top');

set(gca,...
    'XTickLabelRotation', 90,...
    'YLim', ylims,...
    'xcolor', 'k',...
    'ycolor', 'k',...
    'box', 'on',...
    'linewidth', 1);

set(findobj(gca, 'type', 'text'), 'fontsize', 8);
end
