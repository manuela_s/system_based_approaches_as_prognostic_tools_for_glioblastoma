function SmacMimeticsInSilicoClinicalTrialFigure()
% Make figure 4 panels A-C.
% Make SMAC mimetics dose response curves for patients broken down as:
% - responsive to standard therapy;
% - responsive to only standard therapy and SMAC mimetics;
% - non responsive to standard therapy and SMAC mimetics.

% Read in APOPTO-CELL results for SMAC mimetics in silico trial generated
% by run.SmacMimeticsInSilicoClinicalTrial
sim = cohort_utilities.ReadSimulationResults(...
    fullfile('outputs', 'smac_mimetics_in_silico_clinical_trial.mat'),...
    'cancer_type', 'gbm');

% Annotate patients with their predicted response to SMAC mimetics
% supplementation
groups = GetTrialGroups(sim);
trial = innerjoin(sim, groups, 'keys', 'universal_patient_id');

% Make figure
figure();
p = matlab_utilities.panel.panel();

% Create sub-panel for legend underneath, and one column per group
p.pack('v', {[], {5}});
p(1).pack('h', 3);

PlotTrialGroup(...
    p(1, 1),...
    trial(trial.group=='A', :),...
    [0 114 178]./255,...
    'Responsive to standard therapy');

PlotTrialGroup(...
    p(1, 2),...
    trial(trial.group=='B', :),...
    [255 207 0]./255,...
    sprintf('Responsive to only standard therapy\nand SMAC mimetics'));

PlotTrialGroup(...
    p(1, 3),...
    trial(trial.group=='C', :),...
    [196 56 39]./255,...
    sprintf('Non responsive to standard therapy\nand SMAC mimetics'));

% Include a blank line in ylabel to make space for per-row ylabels.
hy = ylabel(p(1), {'Substrate Cleavage at 15 min [%]', ''});
hx = xlabel(p(1), 'Smac mimetics dose [\muM]');
set([hx, hy], 'color', 'k');

% Make a common legend with apoptosis susceptibility cut-off
matlab_utilities.CombineLegends(p(2), num2cell(findobj(gcf, 'type', 'legend')),...
    'create_subpanels', false);

p.fontname = 'arial';
p.fontsize = 8;

p.margintop = 15;
p.marginbottom = 5;
p(1).marginbottom = 10;
p(2).margintop = 10;

p.export(...
    fullfile('figures_and_tables', 'in_silico_smac_mimetics_trial_figure.pdf'),...
    '-rx',...
    '-h73',...
    '-w162');
end

function groups = GetTrialGroups(t)
% Groups patients into 3 response groups to SMAC mimetics:
% - A: responsive to standard therapy;
% - B: responsive to only standard therapy and SMAC mimetics;
% - C: non responsive to standard therapy and SMAC mimetics.
% Input argument:
% - t: table with simulation_results as returned by
%      cohort_utilities.ReadSimulationResults;
% Output:
% - groups: table with columns:
%   - universal_patient_id: categorical, patient identifier from t;
%   - group: categorical, trial group identifier, either 'A', 'B' or 'C'.

groups = matlab_utilities.ConcatenateTables(...
    {GroupAPatients(t), GroupBPatients(t), GroupCPatients(t)},...
    'keep_columns', {'universal_patient_id'},...
    'new_column', 'group',...
    'new_column_values', {'A', 'B', 'C'});

assert(isequal(unique(t.universal_patient_id), sort(groups.universal_patient_id)),...
    'Every patient needs to be in one and only one group');
end

function groupA = GroupAPatients(t)
% Identify patients for Group A (responsive to standard therapy).
% Patients are considered to respond to standard therapy if predicted
% substrate cleavage exceeds 80% after 15 minutes when simulating without
% SMAC mimetics.
% Input argument:
% - t: table, with simulation_results as returned by
%      cohort_utilities.ReadSimulationResults;
% Output:
% - groupA: table, t subset to patients in trial group A.

groupA = t(t.smac_mimetics==0 & t.substrate_cleavage_15 > 80, :);
end

function groupB = GroupBPatients(t)
% Identify patients for Group B (responsive to only standard therapy and SMAC mimetics).
% Patients are considered responsive to only standard therapy and SMAC
% mimetics if both:
% - substrate cleavage is lower or equal to 80% after 15 minutes of
%   simulation without SMAC mimetics supplementation;
% - substrate cleavage is higher than 80% after 15 minutes of simulation
%   when simulated with a concentration of SMAC mimetics higher than 0.
% Input argument: 
% - t: table, with simulation_results as returned by
%      cohort_utilities.ReadSimulationResults;
% Output:
% - groupB: table, t subset to patients in trial group B.

t = t(t.smac_mimetics==0 | t.smac_mimetics==max(t.smac_mimetics), :);
grouped = unstack(t, 'substrate_cleavage_15', 'smac_mimetics',...
    'GroupingVariables', 'universal_patient_id',...
    'NewDataVariableNames', {'substrate_cleavage_15_no_smac',...
    'substrate_cleavage_15_high_smac'});
groupB = grouped(...
    grouped.substrate_cleavage_15_no_smac<=80 &...
    grouped.substrate_cleavage_15_high_smac>80, :);
end

function groupC = GroupCPatients(t)
% Identify patients for Group C (non responsive to standard therapy and SMAC mimetics).
% Patients are considered non responsive to standard therapy and SMAC
% mimetics if substrate cleavage is less or equal to 80% after 15 minutes
% of simulation with or without SMAC mimetics supplementation.
% Input argument: 
% - t: table, with simulation_results as returned by
%      cohort_utilities.ReadSimulationResults;
% Output:
% - groupC: table, t subset to patients in trial group C.

groupC = t(t.smac_mimetics==max(t.smac_mimetics) & t.substrate_cleavage_15<=80, :);
end

function PlotTrialGroup(p, data, color, title_str)
% Make dose-response plot for 1 trial group.
% Input arguments:
% - p: instance of panel class, panel to plot in;
% - data: table, simulation results as returned by 
%   cohort_utilities.ReadSimulationResults; 
% - color: vector, RGB values for lines;
% - title_str: string, title for trial group.

% Create 2 sub-panels:
% - predicted substrate cleavage with no SMAC mimetics;
% - predicted substrate cleavage with SMAC mimetics (1nM-1uM, logarithmic
%   scale).
p.pack('h', {0.1, 0.9});

% Sub-panel for no SMAC mimetics supplementation
p(1).select();
PlotDoseResponseAtZero(data, color);

% Sub-panel for SMAC mimetics supplementation
p(2).select();
PlotDoseResponse(data, color);

p(1).marginright = 2.5;
p(2).marginleft = 2.5;

n = numel(unique(removecats(data.universal_patient_id)));
title(p, sprintf('%s\nn=%d', title_str, n));
end

function PlotDoseResponseAtZero(trial, color)
% Plot substrate cleavage reached with no smac mimetics.
% Input arguments:
% - trial: table, simulation results as returned by 
%   cohort_utilities.ReadSimulationResults; 
% - color: vector, RGB values for lines.

% Subset table for no SMAC mimetics supplementation
trial = trial(trial.smac_mimetics==0, :);

plot(trial.smac_mimetics, trial.substrate_cleavage_15, 's',...
    'MarkerSize', 2,...
    'MarkerFaceColor', color,...
    'MarkerEdgeColor', color,...
    'linewidth', 0.1,...
    'color', color);

set(gca,...
    'XLim', [-0.1, 0.1],...
    'XTick', 0,...
    'YLim', [-2, 102],...
    'YTick', 0:10:100,...
    'box', 'on',...
    'linewidth', 1,...
    'xcolor', 'k',...
    'ycolor', 'k');

r = refline(0, 80);
set(r,...
    'color', 'k',...
    'linestyle', '--',...
    'linewidth', 1,...
    'DisplayName', '80% apoptosis susceptibility cut-off');
end

function PlotDoseResponse(trial, color)
% Plot substrate cleavage reached with range of SMAC mimetics doses.
% Input arguments:
% - trial: table, simulation results as returned by 
%   cohort_utilities.ReadSimulationResults; 
% - color: vector, RGB values for lines.

% Subset table for SMAC mimetics supplementation
trial = trial(trial.smac_mimetics>0, :);

% Unstack to wide table with one column per SMAC concentration:
trial.smac_mimetics = arrayfun(...
    @(s) strrep(sprintf('x%.5f', s), '.', '_'), trial.smac_mimetics,...
    'UniformOutput', false);
j3 = unstack(trial, 'substrate_cleavage_15', 'smac_mimetics',...
    'GroupingVariables', 'universal_patient_id',...
    'AggregationFunction', @cohort_utilities.DummyAggregator);

% Reconstruct numerical concentration from column names
concs = str2double(regexprep(j3.Properties.VariableNames(2:end), 'x(\d)_?', '$1.'));

% Add lines to hggroup to hide from legend
hg = hggroup();
plot(concs, j3{:,2:end}, '-s',...
    'MarkerSize', 2,...
    'MarkerFaceColor', color,...
    'MarkerEdgeColor', color,...
    'linewidth', 0.1,...
    'color', color,...
    'parent', hg);

set(gca,...
    'XScale', 'log',...
    'XLim', [min(concs), max(concs)],...
    'XTick', power(10, log10(min(concs)):log10(max(concs))),...
    'YLim', [-2, 102],...
    'YTick', 0:10:100,...
    'YTickLabel', {},...
    'box', 'on',...
    'linewidth', 1,...
    'xcolor', 'k',...
    'ycolor', 'k');

% Plot reference line at 80% substrate cleavage after 15 min of simulation
r = refline(0, 80);
set(r,...
    'color', 'k',...
    'linestyle', '--',...
    'linewidth', 1,...
    'DisplayName', '80% apoptosis susceptibility cut-off');

legend('show');
end
