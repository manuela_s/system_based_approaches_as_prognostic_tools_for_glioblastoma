function CoxRegressionModelsTable()
% Make table 3.
% Make table with results from univariate and multivariate Cox regression
% models for clinical covariates, single protein signatures and apoptosis
% susceptibility predicted by APOPTO-CELL.

cli = get.InHouseClinicalData();
sim = cohort_utilities.ReadSimulationResults(...
    fullfile('outputs', 'in_house_simulation_results.mat'),...
    'cancer_type', 'gbm');

data = innerjoin(cli, sim,...
    'keys',{'cohort', 'sub_cohort', 'universal_patient_id', 'samples', 'tumour_samples'},...
    'LeftVariables', {'cohort', 'universal_patient_id',...
    'progression_free_survival_months', 'is_censored_progression_free_survival',...
    'age', 'sex', 'location', 'history', 'MGMT_promoter_methylation'},...
    'RightVariables', {'apaf1', 'caspase3', 'caspase9', 'smac', 'xiap',...
    'substrate_cleavage_class'});

% Rename column with APOPTO-CELL predictions as they should appear in table
% and reorder reference category
data.Properties.VariableNames{'substrate_cleavage_class'} = 'apoptosis_susceptibility';
data.apoptosis_susceptibility = reordercats(data.apoptosis_susceptibility,...
    {'SC>80%', 'SC<=80%'});

% Dichotomize patients using median protein expression as cut-off
protein_classes = varfun(...
    @(x) categorical(x>median(x), [true, false], {'>median', '<=median'}),...
    data,...
    'InputVariables', {'apaf1', 'caspase3', 'caspase9', 'smac', 'xiap'});
% Rename columns with protein classes as they should appear in table
protein_classes.Properties.VariableNames = {'apaf1', 'procaspase3', 'procaspase9', 'SMAC', 'XIAP'};

% Replace protein columns in data with categorical columns from protein_classes
data(:, {'apaf1', 'caspase3', 'caspase9', 'smac', 'xiap'}) = []; 
data = [data, protein_classes];

% Run univariate Cox regression models for selected variables
vars = {'age', 'sex', 'location', 'history', 'MGMT_promoter_methylation',...
    'apaf1', 'procaspase3', 'procaspase9', 'SMAC', 'XIAP',...
    'apoptosis_susceptibility'};
for i=1:numel(vars)
   univariate_t{i} = RunSingleCoxModel(data, vars{i}, {});  %#ok<AGROW>
end

% Run the multivariate Cox regression model
multivariate_t = RunSingleCoxModel(data, 'apoptosis_susceptibility',...
    {'age', 'history', 'MGMT_promoter_methylation'});

% Concatenate stats from univariate and multivariate Cox regression models
t = [vertcat(univariate_t{:}); multivariate_t];

% Export to csv
writetable(t,...
    fullfile('figures_and_tables', 'cox_regression_models_table.csv'),...
    'QuoteStrings', true);
end

function model_stats = RunSingleCoxModel(data, varname, adjustment_vars)
% Run single Cox regression model, either univariate or multivariate.
% Input arguments:
% - data: table, one row per patient and columns:
%   - cohort: categorical, grouping variable to run each Cox model for;
%   - progression_free_survival_months: vector of double, survival times;
%   - is_censored_progression_free_survival: categorical, censoring, either
%     'yes' for censored or 'no' for event;
%   and columns for varname and adjustment_vars;
% - varname: string, name of predictor column to run Cox regression model for;
% - adjustment_vars: cell array, predictor column names to use as adjustment
%   variables;
% Output:
% - model_stats:, table with one row for overall stats for the predictor;
%   For categorical predictors there is also one row per level other than
%   the reference level. Columns include:
%   - Predictors: string, description of predictor / predictor level;
%   - HR: string, formatted hazard ratio for predictor or predictor level;
%   - CI: string, formatted 95th percentile confidence interval for HR for
%     predictor / predictor level;
%   - P: string, formatted likelihood ratio test p-value for predictor / 
%     predictor level.

assert(all(ismember(data.is_censored_progression_free_survival, {'yes', 'no'})));

% Run the univariate or multivariate Cox regression model
[~, per_term, per_term_level] = matlab_utilities.RunMultipleCoxModelsR(...
    data.cohort,...
    data.progression_free_survival_months,...
    data.is_censored_progression_free_survival=='no',...
    data(:, [{varname}, adjustment_vars]));

% Subset to stats only for the "primary" predictor
per_term = per_term(strcmp(per_term.cox_term, varname), :);
per_term_level = per_term_level(strcmp(per_term_level.cox_term, varname), :);

% Format likelihood ratio test p-value for the "primary" predictor
term_p = regexprep(matlab_utilities.FormatPValue(per_term.lrt_p), 'p=?', '');

% Format hazard-ratio and confidence intervals for each term level
% (single row for continuous variables)
level_hr = arrayfun(@(hr) sprintf('%.2f', hr), per_term_level.hr, 'UniformOutput', false);
level_ci = rowfun(@(low_ci, high_ci) sprintf('%.2f-%.2f', low_ci, high_ci),...
    per_term_level,...
    'InputVariables', {'low_ci', 'high_ci'},...
    'OutputFormat', 'cell');

% Format predictor string:
% - replace '_' with ' ';
% - capitalize variable names;
% - prefix with "Adjusted" for multivariate Cox model.
if isempty(adjustment_vars)
    pretty_varname = regexprep([upper(varname(1)),varname(2:end)], '_', ' ');
else
    pretty_varname = sprintf('Adjusted %s', regexprep(varname, '_', ' '));
end
    
if iscategorical(data.(varname))
    % Format description of predictor term and term levels for categorical
    % variables
    term_predictor = sprintf('%s (ref. %s, n=%d)', pretty_varname,...
        per_term.ref_level{1}, sum(data.(varname)==per_term.ref_level{1}));
    level_predictors = cellfun(@(s) sprintf('    %s (n=%d)', s,...
        sum(data.(varname)==s)), per_term_level.cox_term_level,...
        'UniformOutput', false);
    
    % Build output table with 1 row for overall stats for predictor and one
    % additional row for each predictor level other than the reference level
    model_stats = table(...
        [{term_predictor}; level_predictors],...
        [{''}; level_hr],...
        [{''}; level_ci],...
        [{term_p}; repmat({''}, height(per_term_level), 1)],...
        'VariableNames', {'Predictors', 'HR', 'CI', 'P'});
else
    % Format description of predictor and build output table
    predictor = sprintf('%s (continuous, n=%d)', pretty_varname, sum(~isnan(data.(varname))));
    model_stats = table(...
        {predictor},...
        level_hr,...
        level_ci,...
        {term_p},...
        'VariableNames', {'Predictors', 'HR', 'CI', 'P'});
end
end
