function ClinicalCharacteristicsTable()
% Make table 2.
% Make table with statistics for each clinical feature grouped by type of
% tumour_samples (newly-diagnosed and recurrent tumours).

% Get clinical data
cli = get.InHouseClinicalData();

grouping_var = {'tumour_samples'};

wstate = matlab_utilities.TemporarilyDisableWarnings('MATLAB:table:ModifiedVarnames'); %#ok<NASGU>

% Creates headings for the table
patients_stats = grpstats(cli, grouping_var, [], 'DataVars', false);
patients_stats.tumour_samples = renamecats(patients_stats.tumour_samples,...
    {'newly_diagnosed', 'recurrent'},...
    {'Newly-diagnosed', 'Recurrent'});
patients_stats.combined_str = cellfun(...
    @(str,n,percentage) sprintf('%s (n=%d, %2.0f%%)', str, n, percentage),...
    cellstr(patients_stats.tumour_samples),...
    num2cell(patients_stats.GroupCount),...
    num2cell(100*patients_stats.GroupCount/height(cli)),...
    'UniformOutput', false);
patients_stats = unstack(patients_stats(:, {'tumour_samples', 'combined_str'}),...
    'combined_str', 'tumour_samples');
patients_stats.col = repmat({''}, height(patients_stats), 1);
patients_stats.Properties.RowNames = {};
patients_stats.Properties.VariableNames{'Newly_diagnosed'} = 'newly_diagnosed';
patients_stats.Properties.VariableNames{'Recurrent'} = 'recurrent';

% Create the body of the table, 1 set of rows per clinical feature
cols = {'age', 'sex', 'location', 'MGMT_promoter_methylation', 'treatment'};
for i=1:numel(cols)
    t_out{i} = MakeTableRow(cli, grouping_var, cols{i}); %#ok<AGROW>
end
t = vertcat(t_out{:});
t.Properties.RowNames = {};

% Computes median progression-free survival broken down by tumour_samples
pfs_stats = matlab_utilities.ComputeMedianSurvivalR(...
    cli.tumour_samples,...
    cli.progression_free_survival_months,...
    cli.is_censored_progression_free_survival=='no');
pfs_stats.combined_str = arrayfun(...
    @(m,lci,hci) sprintf('%.1f (%.1f-%.1f)', m, lci, hci),...
    pfs_stats.median_survival, pfs_stats.low_ci, pfs_stats.high_ci,...
    'UniformOutput', false);
pfs_stats = unstack(pfs_stats(:, {'sample_set', 'combined_str'}),...
    'combined_str', 'sample_set');
pfs_stats.col = {'PFS (median, 95%CI) [months]'};

% Combine headings, body of the table and progression-free survival median table 
t = [patients_stats; t; pfs_stats];
t = t(:, {'col', 'newly_diagnosed', 'recurrent'});

% Export table as csv file
writetable(t,...
    fullfile('figures_and_tables', 'clinical_characteristics_table.csv'),...
    'QuoteStrings', true,...
    'WriteVariableNames', false);
end

function t_out = MakeTableRow(cli, grouping_var, col)
% Process single variable.
% Input arguments:
% - cli: table, clinical data as returned by get.InHouseClinicalData;
% - grouping_var: string, column name to group table by;
% - col: string, column name to process;
% Output:
% - t_out: table, columns:
%   - col: description of the predictor/category level.
%   - newly_diagnosed/recurrent: median/range for continuous predictors and n/% for
%     categorical predictors.

% Capitalize predictor names and remove underscores
pretty_col = regexprep([upper(col(1)), col(2:end)], '_', ' ');

if iscategorical(cli.(col))
    cli.(col)(isundefined(cli.(col))) = 'not available';
    grouping_var_stats = grpstats(cli, grouping_var, [], 'DataVars', false);
    stats = grpstats(cli, [grouping_var, {col}], [], 'DataVars', false);
    stats = innerjoin(stats, grouping_var_stats, 'keys', grouping_var);
    stats.str = rowfun(@(n, tot) sprintf('%2d (%2.0f%%)', n, 100*n/tot), stats,...
        'InputVariables', {'GroupCount_stats', 'GroupCount_grouping_var_stats'},...
        'OutputFormat', 'cell');
    t_out = unstack(stats, 'str', grouping_var, 'GroupingVariables', col);
    t_out.(col) = cellfun(@(s) sprintf('     %s', s), cellstr(t_out.(col)),...
        'UniformOutput', false);
    t_out = [...
        cell2table([{pretty_col}, repmat({''}, 1, width(t_out) - 1)],...
        'VariableNames', t_out.Properties.VariableNames);...
        t_out];
    t_out.Properties.VariableNames{col} = 'col';
else
    stats = grpstats(cli, grouping_var, {@nanmedian, @nanmin, @nanmax},...
        'DataVars', col,...
        'VarNames', [grouping_var, {'GroupCount', 'median', 'min', 'max'}]);
    stats.str = rowfun(...
        @(median_col, min_col, max_col) sprintf('%.0f (%.0f-%.0f)',...
        median_col, min_col, max_col),...
        stats,...
        'InputVariables', {'median', 'min', 'max'},...
        'OutputFormat', 'cell');
    if strcmp(col, 'age')
        col_pretty_str = cellstr(sprintf('%s (median, range) [years]', pretty_col));
    else
        col_pretty_str = cellstr(sprintf('%s (median, range)', pretty_col));
    end
    stats.col = repmat(col_pretty_str, height(stats), 1);
    t_out = unstack(stats, 'str', grouping_var, 'GroupingVariables', 'col');
end
end
