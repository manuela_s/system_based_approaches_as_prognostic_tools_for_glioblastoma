Code book for in_house_apoptocell_proteins_data.csv

- cohort: name of cohort, 'GBM';
- sub_cohort: name of sub_cohort, either
  - 'this_study': de novo patients included in the analysis;
  - 'MurphyCDDis2013': from Murphy et al., Cell Death Disease, 2013;
- universal_patient_id: unique id for each patient;
- samples: unique id for each sample. For patients from 'MurphyCDDis2013'
  sub_cohort, these identifiers reflect the patient numbers/ids used in 
  Murphy et al, Cell Death Disease, 2013;
- sample_type: type of sample, 'patient';
- tumour_samples: type of tumour sample used to measure the protein inputs 
  to APOPTO-CELL, either
  - 'newly_diagnosed';
  - 'recurrent';
- technique: type of technique used to measure inputs to APOPTO-CELL, 'WB';
- apaf1: concentration (in \muM) measured by Western blotting;
- caspase3: concentration (in \muM) measured by Western blotting;
- caspase9: concentration (in \muM) measured by Western blotting;
- smac: concentration (in \muM) measured by Western blotting;
- xiap: concentration (in \muM) measured by Western blotting.

For more information, refer to the review article.
