Code book for in_house_clinical_data.csv

- cohort: name of cohort, 'GBM';
- sub_cohort: name of sub_cohort, either
  - 'this_study': de novo patients included in the analysis;
  - 'MurphyCDDis2013': from Murphy et al., Cell Death Disease, 2013;
- universal_patient_id: unique id for each patient;
- samples: unique id for each sample. For patients from 'published(MurphyCDDis2013)'
  sub_cohort, these identifiers reflect the patient numbers/ids used in 
  Murphy et al, Cell Death Disease, 2013;
- tumour_samples: type of tumour sample used to measure the protein inputs 
  to APOPTO-CELL, either 
  - 'newly_diagnosed';
  - 'recurrent';
- treatment: treatment received by the patient, either
  - 'none';
  - 'TMZ + radiotherapy';
  - 'TMZ + radiotherapy + Avastin + Irinotecan';
  - 'TMZ + radiotherapy + Cilengitide';
- history: combined tumour sample/treatment history per patient, either
  - 'newly-diagnosed - no treatment';
  - 'recurrent - no treatment';
  - 'recurrent - treatment';
- MGMT_promoter_methylation: methylation status of the MGMT promoter, either
  - 'methylated';
  - 'unmethylated';
  - empty, if not assessed;
- age: patient age, in years;
- sex: patient sex, either
  - 'F': female;
  - 'M': male;
- location: location of the tumour the samples was taken from, either
  - 'left side': 'Ltcentral', 'Ltfrontal', 'Ltoccipital', 'Ltparietal' or 'Lttemporal';
  - 'right side': Rtfrontal', 'Rtfrontotemporal', 'Rtoccipital', 'Rtparietal',
    'Rtparietooccipital', 'Rtprecentral', 'Rttemporal', 'Rttemporooccipital'
     or 'Rttemporoparietal';
  - 'other': 'Bifrontal', 'Frontal' or 'Supratentorial';
  - empty, if information was not available;
- progression_free_survival_months: time interval between surgical resection
  of the tumor (either newly-diagnosed or recurrent) and progression, loss
  to follow-up or study end-date, in months;
- is_censored_progression_free_survival: censoring,
  - 'yes': if patient did not experience a progression of the disease before
    being lost to follow-up or study end-date;
  - 'no': if patient had a documented disease progression before being lost
    to follow-up or study end-date.

For more information, refer to the review article.
