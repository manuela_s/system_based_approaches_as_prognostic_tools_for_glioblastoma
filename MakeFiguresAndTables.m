function MakeFiguresAndTables()
% Generate figures and tables included in the manuscript.

if exist('figures_and_tables', 'dir')==0
    mkdir('figures_and_tables');
end

% Table 2
make.ClinicalCharacteristicsTable();
% Table 3
make.CoxRegressionModelsTable();

% Figure 1
make.ProteinExpressionBoxplotFigure();
% Figure 2
make.ProteinExpressionKaplanMeierFigure();
% Figure 3
make.ApoptoCellPredictionsFigure();
% Figure 4
make.SmacMimeticsInSilicoClinicalTrialFigure();
end
