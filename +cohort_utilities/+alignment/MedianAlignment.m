classdef MedianAlignment < cohort_utilities.alignment.AbstractAligner
    % MedianAlignment.
    % Align a new dataset (to_be_matched_dataset) to a reference dataset by
    % scaling the values of the new dataset to have the same median as the
    % reference dataset.

    properties
        % scalar, scaling factor to match median in new dataset to the
        % median in the reference dataset
        median_factor;
    end
    
    methods
        function obj = MedianAlignment(reference_dataset, to_be_matched_dataset)
            % Create instance of MedianAlignment.
            % Input arguments:
            % - reference_dataset: numeric vector, measurements from the
            %   reference_dataset to align to;
            % - to_be_matched_dataset: numeric vector, measurements from the
            %   new dataset to be aligned to the reference dataset;
            % Output:
            % - obj: instance of MedianAlignment, can be used to transform 
            %   values that are in or aligned with to_be_matched_dataset to
            %   values in the reference_dataset.
            % See also Transform.
            obj.median_factor = nanmedian(reference_dataset) ./ nanmedian(to_be_matched_dataset);
        end
        
        function new_values = Transform(obj, old_values)
            new_values = old_values .* obj.median_factor;
        end
    end
    
    methods (Static)
        function new_values = Align(reference_dataset, to_be_matched_dataset)
            obj = cohort_utilities.alignment.MedianAlignment(reference_dataset, to_be_matched_dataset);
            new_values = obj.Transform(to_be_matched_dataset);
        end
    end 
end
