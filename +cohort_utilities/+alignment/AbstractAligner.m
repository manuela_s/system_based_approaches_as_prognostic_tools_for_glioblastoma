classdef AbstractAligner
    % AbstractAligner
    % Superclass for aligner-classes that can tranform data to align it to
    % a reference distribution.
    
    methods (Abstract)
        % Convert values to match reference distribution.
        % Input argument:
        % - old_values: array with the values that need to be aligned to the
        %   reference cohort;
        % Output:
        % - new_values: array with the values aligned to the reference
        %   cohort.
        new_values = Transform(obj, old_values);
    end
    
    methods (Abstract, Static)
        % Perform alignment of new data to the reference dataset.
        % Input arguments:
        % - reference_dataset: numeric vector, measurements from the
        %   reference_dataset to align to; 
        % - to_be_matched_dataset: numeric vector, measurements from the
        %   new dataset to be aligned to the reference dataset;
        % Output:
        % - new_values: numeric vector, measurements from the
        %   to_be_matched_dataset aligned to the reference_dataset.
        new_values = Align(reference_dataset, to_be_matched_dataset);
    end 
end

