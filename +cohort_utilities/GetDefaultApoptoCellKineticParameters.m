function p = GetDefaultApoptoCellKineticParameters()
% Create a struct with parameters for APOPTO-CELL simulation. 
% See Rehm et al., EMBO, 2006 for details.

% 1 C3_ + 1 C9a => 1 C3a + 1 C9a
p.k3 = 6.0;
% 1 C9a + 1 C3a => 1 C9H + 1 C3a
p.k4 = 12.0;
% 1 C3_ + 1 C3a => 1 C3a + 1 C3a
p.k6 = 2.4;
% 1 C3a + 1 XIAP_ <=> 1 [XIAP~C3a]
p.k7 = 156.0;
p.k_7 = 0.144;
% 1 C3a + 1 BIR12_ <=> 1 [BIR12~C3a]
p.k8 = 156.0; 
p.k_8 = 0.144;
% 1 XIAP_ + 1 C3a => 1 BIR12_ + 1 BIR3R_ + 1 C3a
p.k9 = 12.0;
% 1 [XIAP~C9a] + 1 C3a => 1 BIR12_ + 1 [BIR3R~C9a] + 1 C3a
p.k10 = 12.0;
% 1 [XIAP~C3a] + 1 C3a => 1 BIR3R_ + 1 [BIR12~C3a] + 1 C3a
p.k11 = 12.0;
% 1 [XIAP~{cl-C9a}] + 1 C3a => 1 BIR12_ + 1 [BIR3R~{cl-C9a}] + 1 C3a
p.k12 = 12.0;
% 1 [[XIAP~{cl-C9a}]~C3a] + 1 C3a => 1 [BIR12~C3a] + 1 [BIR3R~{cl-C9a}] + 1 C3a
p.k13 = 12.0;
% 1 [XIAP~C9a~C3a] + 1 C3a => 1 [BIR12~C3a] + 1 [BIR3R~C9a] + 1 C3a
p.k14 = 12.0;
% 1 [XIAP~2SMAC] + 1 C3a => 1 [BIR12~SMAC] + 1 [BIR3R~SMAC] + 1 C3a
p.k15 = 12.0;
% 1 [XIAP~C9a~C3a] + 1 C3a => 1 C9H + 1 [[XIAP~{cl-C9a}]~C3a] + 1 C3a
p.k16 = 12.0;
% 1 [XIAP~C9a] + 1 C3a => 1 C9H + 1 [XIAP~{cl-C9a}] + 1 C3a
p.k17 = 12.0;
% 1 [BIR3R~C9a] + 1 C3a => 1 [BIR3R~{cl-C9a}] + 1 C9H + 1 C3a
p.k18 = 12.0;
% 1 C9a + 1 XIAP_ <=> 1 [XIAP~C9a]
p.k19 = 156.0; p.k_19 = 0.144;
% 1 C9a + 1 BIR3R_ <=> 1 [BIR3R~C9a]
p.k20 = 156.0; p.k_20 = 0.144;
% 1 XIAP_ + 1 SMAC_ + 1 SMAC_ <=> 1 [XIAP~2SMAC]
p.k21 = 420.0; p.k_21 = 0.133;
% 1 [XIAP~C9a] + 1 SMAC_ + 1 SMAC_ <=> 1 [XIAP~2SMAC] + 1 C9a
p.k22 = 420.0; p.k_22 = 156.0;
% 1 [XIAP~C3a] + 1 SMAC_ + 1 SMAC_ <=> 1 [XIAP~2SMAC] + 1 C3a
p.k23 = 420.0; p.k_23 = 156.0;
% 1 BIR12_ + 1 SMAC_ <=> 1 [BIR12~SMAC]
p.k24 = 4.45; p.k_24 = 31.9;
% 1 BIR3R_ + 1 SMAC_ <=> 1 [BIR3R~SMAC]
p.k25 = 0.33; p.k_25 = 14.2;
% 1 [BIR12~C3a] + 1 SMAC_ <=> 1 [BIR12~SMAC] + 1 C3a
p.k26 = 4.45; p.k_26 = 156.0;
% 1 [BIR3R~C9a] + 1 SMAC_ <=> 1 [BIR3R~SMAC] + 1 C9a
p.k27 = 0.33; p.k_27 = 156.0;
% 1 [XIAP~{cl-C9a}] + 1 SMAC_ + 1 SMAC_ <=> 1 [[XIAP~{cl-C9a}]~SMAC]
p.k28 = 420.0; p.k_28 = 156.0;
% 1 C9H => out; 1 C9a => out; 1 C3a => out; 1 [XIAP~{cl-C9a}] => out; 1 BIR12_ => out;
% 1 [BIR12~SMAC] => out; 1 [BIR12~C3a] => out; 1 [BIR3R~C9a] => out;
% 1 SMAC_ => out; 1 SMAC_mimetic => out; 1 PAC => out; 1 [BIR12~SMACmimetic] => out; 
p.kout1 = 0.0058;
% 1 [XIAP~C3a] => out; 1 [XIAP~C9a~C3a] => out; 1 [XIAP~C9a] => out;
% 1 [[XIAP~{cl-C9a}]~C3a] => out; 1 [[XIAP~{cl-C9a}]~SMAC] => out;
% 1 [XIAP~2SMAC] => out; 1 BIR3R_ => out; 1 [BIR3R~SMAC] => out;
% 1 [BIR3R~{cl-C9a}] => out; 1 [XIAP~2SMACmimetic] => out; 
% 1 [BIR3R~SMACmimetic] => out; 1 [[XIAP~{cl-C9a}]~SMACmimetic] => out;
p.kout2 = 0.0347;
% 1 Substrate + 1 C3a => 1 C3a
p.k47 = 12.0;
% 1 C3_ => out
p.k50 = 0.0039;
% 1 XIAP_ => out
p.k51 = 0.0116;
% 1 XIAP_ + 1 SMAC_mimetic + 1 SMAC_mimetic <=> 1 [XIAP~2SMACmimetic]
p.k53 = 420.0; 
p.k_53 = 0.133;
% 1 [XIAP~C9a] + 1 SMAC_mimetic + 1 SMAC_mimetic <=> 1 [XIAP~2SMACmimetic] + 1 C9a
p.k54 = 420.0; 
p.k_54 = 156.0;
% 1 [XIAP~C3a] + 1 SMAC_mimetic + 1 SMAC_mimetic <=> 1 [XIAP~2SMACmimetic] + 1 C3a
p.k55 = 420.0;
p.k_55 = 156.0;
% 1 BIR12_ + 1 SMAC_mimetic <=> 1 [BIR12~SMACmimetic]
p.k56 = 4.45;
p.k_56 = 31.9;
% 1 BIR3R_ + 1 SMAC_mimetic <=> 1 [BIR3R~SMACmimetic]
p.k57 = 0.33;
p.k_57 = 14.2;
% 1 [BIR12~C3a] + 1 SMAC_mimetic <=> 1 [BIR12~SMACmimetic] + 1 C3a
p.k58 = 4.45;
p.k_58 = 156.0;
% 1 [BIR3R~C9a] + 1 SMAC_mimetic <=> 1 [BIR3R~SMACmimetic] + 1 C9a
p.k59 = 0.33;
p.k_59 = 156.0;
% 1 [XIAP~{cl-C9a}] + 1 SMAC_mimetic + 1 SMAC_mimetic <=> 1 [[XIAP~{cl-C9a}]~SMACmimetic]
p.k60 = 420.0;
p.k_60 = 156.0;
% 1 PAC + 1 C3_ => 1 C3a + 1 PAC
p.k61 = 0.068;
% 1 [XIAP~2SMACmimetic] + 1 C3a => 1 [BIR12~SMACmimetic] + 1 [BIR3R~SMACmimetic] + 1 C3a
p.k67 = 12;
end
