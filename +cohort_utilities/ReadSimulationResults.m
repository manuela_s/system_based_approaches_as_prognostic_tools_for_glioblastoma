function t = ReadSimulationResults(matfile_name, varargin)
% Read APOPTO-CELL simulation results.
% Input arguments:
% - matfile_name: name of the matfile the simulation results are stored in;
% - key/value pairs: 
%   - cancer_type: either crc or gbm. 
% Output:
% - t: table with:
%   - all columns in the input to cohort_utilities.SimulateCohort();
%   - substrate_cleavage_end: substrate_cleavage at the end of the simulation;
%   - substrate_cleavage_mean: mean substrate_cleavage over the duration of the simulation;
%   - substrate_cleavage_timecourse: vector per row with the 
%     substrate cleavage time-course over the duration of the simulation;
%   - substrate_cleavage_15: substrate_cleavage after 15 minutes of
%     simulations. Computed only for cancer_type GBM;
%   - substrate_cleavage_class: 
%     - for cancer_type CRC: categorical (SC>25%, SC <= 25%) based on
%       substrate_cleavage_end and cut-off of 25%;
%     - for cancer_type GBM: categorical (SC>80%, SC <= 80%) based on
%       substrate_cleavage_15 and cut-off of 80%;

p = inputParser();
p.addParameter('cancer_type', 'crc',...
    @(cancer_type) ismember(cancer_type, {'crc', 'gbm'}));
p.parse(varargin{:});

load(matfile_name);

% Create table from inputs and cleavage in results:
t = results.inputs;
t.substrate_cleavage_end = arrayfun(@(r) r.traces(end, 3), results.results');
t.substrate_cleavage_mean = arrayfun(@(r) mean(r.traces(:, 3)), results.results');
t.substrate_cleavage_timecourse = arrayfun(@(r) r.traces(:, 3), results.results',...
    'UniformOutput', false);

if strcmp(p.Results.cancer_type, 'crc')
    % CRC
    % Apoptosis susceptibility (substrate_cleavage_class) is defined using
    % a cut-off value of 25% substrate cleavage at the end of simulation 
    % (300 min) to dichotomize patients in apoptosis-sensitive (SC>25%) or
    % apoptosis-resistant (SC<=25%). 
    % See also Hector et al., GUT, 2012 and Salvucci et al., Clinical Cancer
    % Research, 2016.
    t.substrate_cleavage_class = categorical(...
        matlab_utilities.iif(t.substrate_cleavage_end>25, 'SC>25%', 'SC<=25%'));
    if numel(categories(t.substrate_cleavage_class)) == 2
        t.substrate_cleavage_class = reordercats(...
            t.substrate_cleavage_class, {'SC<=25%', 'SC>25%'});
    end
else
    % GBM
    % Apoptosis susceptibility (substrate_cleavage_class) is defined using
    % a cut-off value of 80% substrate cleavage after 15 min from simulation
    % start point to dichotomize patients in apoptosis-sensitive (SC>80%)
    % or apoptosis-resistant (SC<=80%).
    % Note that Murphy et al., Cell Death Disease, 2013 use a cut-off of 80%
    % substrate cleavage reached at 60 min after simulation start point.
    t.substrate_cleavage_15 = arrayfun(@(r) r.traces(16, 3), results.results');
    t.substrate_cleavage_class = categorical(...
        matlab_utilities.iif(t.substrate_cleavage_15>80, 'SC>80%', 'SC<=80%'));
    if numel(categories(t.substrate_cleavage_class)) == 2
        t.substrate_cleavage_class = reordercats(t.substrate_cleavage_class,...
            {'SC<=80%', 'SC>80%'});
    end
end

end
