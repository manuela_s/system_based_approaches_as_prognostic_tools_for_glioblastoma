function simulations = SimulateCohort(data, varargin)
% Run APOPTO-CELL simulations for a cohort.
% Input arguments:
% - data: MATLAB table containing protein concentrations in uM. Needs to include columns:
%   'AbsCaspase3', 'AbsCaspase9', 'AbsXiap', 'AbsApaf_1', 'AbsSmac' or
%   'caspase3', 'caspase9', 'xiap', 'apaf1', 'smac'.
%   If 'include_smac_mimetics' parameter is set to true, the table must
%   also include a column named 'smac_mimetics' with the concentration of SMAC mimetics.
% - key/value pairs:
%   - keep_traces: logical, include traces of key species in results;
%   - keep_all_traces: logical, include traces for all species in results;
%   - include_smac_mimetics: logical, include SMAC mimetics in the simulation;
%   - duration: time to simulate (in minutes);
%   - progressbar_label: string, label to use for progressbar.
% Outputs:
% - simulations: struct with simulation results. It has the following fields:
%   - inputs: MATLAB table with the data that SimulateCohort was run with;
%   - results: results of the simulations. Struct with fields:
%     - traces: matrix with time-courses of key species. See traces_headers.
%       Field only present if keep_traces parameter is set to true.
%     - all_traces: matrix with time-courses of extended set of species. See all_traces_headers.
%       Field only present if keep_all_traces parameter is set to true.
%     - substrate_cleavage_end: % of substrate cleavage reached at the end
%       of simulation as specified by settings.duration.
%   - settings: struct with the settings the simulation was run with. Fields:
%     - duration: minutes to simulate;
%     - stepsize: interval (in minutes) between time points in traces.
%   - traces_headers: description of the columns in results.traces;
%   - all_traces_headers: description of the columns in results.all_traces

p = inputParser();
p.addParameter('keep_traces', true, @islogical);
p.addParameter('keep_all_traces', false, @islogical);
p.addParameter('include_smac_mimetics', false, @islogical);
p.addParameter('duration', 300, @isnumeric);
p.addParameter('progressbar_label', 'SimulateCohort', @ischar);
p.parse(varargin{:});

settings.duration = p.Results.duration;%[min]
settings.stepsize = 1;%[min]

if any(ismember({'AbsCaspase3', 'AbsCaspase9', 'AbsXiap', 'AbsApaf_1', 'AbsSmac'}, data.Properties.VariableNames))
    proteins = data{:, {'AbsCaspase3', 'AbsCaspase9', 'AbsXiap', 'AbsApaf_1', 'AbsSmac'}};
else
    proteins = data{:, {'caspase3', 'caspase9', 'xiap', 'apaf1', 'smac'}};
end
if p.Results.include_smac_mimetics
    proteins(:, 6) = data.smac_mimetics;
else
    if ismember('smac_mimetics', data.Properties.VariableNames)
        error('include_smac_mimetics is not set, but table has a smac_mimetics column');
    end
    proteins(:, 6) = 0;
end
assert(~any(isnan(proteins(:))));
% Set protein concentrations for all proteins except smac_mimetics to be a
% minimum of 0.1 pM
proteins(:, 1:5) = max(proteins(:, 1:5), 1e-7);

pb = matlab_utilities.ProgressBar(size(proteins, 1),...
    p.Results.progressbar_label, 'enable_gui', false);
parfor i = 1:size(proteins, 1)
    [traces, ~, allTraces] = cohort_utilities.ApoptoCell(proteins(i, :), settings);
    if (p.Results.keep_traces)
        results(i).traces = traces;
    end
    if (p.Results.keep_all_traces)
        results(i).allTraces = allTraces;
    end
    results(i).substrate_cleavage_end = traces(end, 3);
    pb.update();
end
clear pb;
traces_headers = {'Casp. 9 [uM]', 'Casp. 3 [uM]', 'cl. Substrate [%]'};
all_traces_headers = {'C3', 'C9a', 'C3a', 'C9H', 'XIAP', '[XIAP~C3a]', '[XIAP~C9a]',...
    '[XIAP~{cl-C9a}]', 'BIR12', 'BIR3R', '[BIR12~C3a]',...
    '[BIR3R~C9a]', '[BIR3R~{cl-C9a}]', 'SMAC_', '[XIAP~2SMAC]', '[[XIAP~{cl-C9a}]~SMAC]',...
    '[BIR12~SMAC]', '[BIR3R~SMAC]', 'Substrate', 'APAF1', 'SMACmito'};
simulations.inputs = data;
simulations.results = results;
simulations.settings = settings;
simulations.traces_headers = traces_headers;
simulations.all_traces_headers = all_traces_headers;
end
