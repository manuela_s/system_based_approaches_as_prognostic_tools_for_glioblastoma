function simulation_inputs = InHouseApoptoCellSimulationInputs()
% Prepare inputs for APOPTO-CELL simulations.
% Read in proteins data from csv for 'MurphyCDDis2013' and 'this_study'
% sub_cohorts, and align median protein concentration of the newly_diagnosed
% samples from this_study sub_cohort to the median protein concentration of
% the sub_cohort from <a href="https://www.ncbi.nlm.nih.gov/pubmed/23681224">Murphy et al., Cell Death Disease, 2013</a>.
% Output:
% - simulation_inputs: table, one row per patient and columns:
%   - cohort: categorical, name of cohort, 'GBM';
%   - sub_cohort, categorical, name of sub_cohort, 'MurphyCDDis2013' or 'this_study';
%   - universal_patient_id: categorical, unique id for each patient;
%   - samples: categorical, unique id for each sample. For patients from 
%     'MurphyCDDis2013' sub_cohort, these identifiers reflect the patient 
%     numbers/ids used in Murphy et al, Cell Death Disease, 2013);
%   - sample_type: categorical, type of sample, 'patient';
%   - tumour_samples: categorical, type of tumour sample used to measure the 
%     protein inputs to APOPTO-CELL, either 'newly_diagnosed' or 'recurrent';
%   - technique: categorical, type of technique used to measure and adjust
%     inputs to APOPTO-CELL, either 'WB' or 'WB_median_aligned_to_Murphy_subcohort';
%   - apaf1: double, apaf1 concentration in uM;
%   - caspase3: double, caspase3 concentration in uM;
%   - caspase9: double, caspase9 concentration in uM;
%   - smac: double, smac concentration in uM;
%   - xiap: double, xiap concentration in uM.

protein_inputs = readtable(fullfile('data', 'in_house_apoptocell_proteins_data.csv'));
cat_vars = {'cohort', 'sub_cohort', 'universal_patient_id', 'samples',...
    'sample_type', 'tumour_samples', 'technique'};
for c = 1:numel(cat_vars)
    protein_inputs.(cat_vars{c}) = categorical(protein_inputs.(cat_vars{c}));
end

aligned_protein_inputs = AlignSubCohortsProteinInputs(protein_inputs);

% Returns simulation inputs
simulation_inputs = aligned_protein_inputs;
end

function aligned_dataset = AlignSubCohortsProteinInputs(dataset)
% Remove batch-effects between 'MurphyCDDis2013' and 'this_study' (transformed_dataset)
% sub_cohorts by aligning the median protein concentration in newly_diagnosed 
% samples from 'this_study' sub_cohort (to_be_matched_dataset) to the median
% protein concentration of the newly_diagnosed samples measured in the sub_cohort
% from 'MurphyCDDis2013'. 
% Recurrent patients from 'this_study' sub_cohort will be also scaled, but 
% transformation factor is based on aligning newly_diagnosed samples only.
% Input argument:
% - dataset: table, one row per patient. Columns include:
%   - sub_cohort: categorical, name of sub_cohort:
%     - 'MurphyCDDis2013' for reference_dataset;
%     - 'this_study' for to_be_matched_dataset;
%   - tumour_samples: categorical, type of tumour sample used to measure the 
%     protein inputs to APOPTO-CELL, either 'newly_diagnosed' or 'recurrent';
%   - technique: categorical, type of technique used to measure inputs to 
%     APOPTO-CELL, 'WB';
%   - apaf1, caspase3, caspase9, smac, xiap: double, protein concentrations,
%     in uM;
% Output:
% - aligned_dataset: table, same shape and content as the input dataset with
%   the following changes:
%   - protein concentrations for patients of the 'this_study' sub_cohort are
%     aligned to the 'MurphyCDDis2013' sub_cohort;
%   - technique set to 'WB_median_aligned_to_Murphy_subcohort' for patients
%     of the 'this_study' sub_cohort.

murphy_dataset = dataset(dataset.sub_cohort=='MurphyCDDis2013', :);
reference_dataset = dataset(...
    dataset.sub_cohort=='MurphyCDDis2013' &...
    dataset.tumour_samples=='newly_diagnosed', :);
to_be_matched_dataset = dataset(...
    dataset.sub_cohort=='this_study' & dataset.tumour_samples=='newly_diagnosed', :);
transformed_dataset = dataset(dataset.sub_cohort=='this_study', :);

proteins = {'apaf1', 'caspase3', 'caspase9', 'smac', 'xiap'};
for p = 1:numel(proteins)
    m = cohort_utilities.alignment.MedianAlignment(...
        reference_dataset.(proteins{p}),...
        to_be_matched_dataset.(proteins{p}));
    transformed_dataset.(proteins{p}) = m.Transform(transformed_dataset.(proteins{p}));
end

transformed_dataset.technique = categorical(repmat({'WB_median_aligned_to_Murphy_subcohort'},...
    height(transformed_dataset), 1));
aligned_dataset = [murphy_dataset; transformed_dataset;];
end
