function cli = InHouseClinicalData()
% Get clinical data for in-house GBM patients from both 'this_study' and
% <a href="https://www.ncbi.nlm.nih.gov/pubmed/23681224">Murphy et al., Cell Death Disease, 2013</a>
% sub_cohorts.
% See also code book in_house_clinical_data.md
% Returns a MATLAB table with 1 row per patient.

cli = readtable(fullfile('data', 'in_house_clinical_data.csv'));
cat_vars = {'cohort', 'sub_cohort', 'universal_patient_id', 'samples',...
    'tumour_samples', 'sex', 'location',...
    'treatment', 'history', 'MGMT_promoter_methylation',...
    'is_censored_progression_free_survival'};
for c = 1:numel(cat_vars)
    cli.(cat_vars{c}) = categorical(cli.(cat_vars{c}));
end

% Reorder categories for selected metadata
cli.sub_cohort = reordercats(cli.sub_cohort, {'this_study', 'MurphyCDDis2013'});
cli.sex = reordercats(cli.sex, {'M', 'F'});
cli.location = reordercats(cli.location, {'left side', 'right side', 'other'});
end
