function SmacMimeticsInSilicoClinicalTrial()
% Run APOPTO-CELL model for each patient of the in-house GBM cohort
% and simulate administration of SMAC mimetics for a range of physiological
% concentrations.

% Get APOPTO-CELL protein inputs
simulation_inputs = get.InHouseApoptoCellSimulationInputs();

% Repeat simulation_inputs for each SMAC mimetics doses including 0 and
% logarithmic range from 1 nM to 1 uM with 25 intervals
drug_range = [0, logspace(-3, 0, 25)];
simulation_inputs = repmat(simulation_inputs, numel(drug_range), 1);
simulation_inputs.smac_mimetics = reshape(...
    repmat(drug_range, height(simulation_inputs)/numel(drug_range), 1), [], 1);

% Run APOPTO-CELL simulations
results = cohort_utilities.SimulateCohort(...
    simulation_inputs,...
    'include_smac_mimetics', true,...
    'duration', 60,...
    'progressbar_label', 'SmacMimeticsInSilicoClinicalTrial'); %#ok<NASGU>

% Save simulation results
save(fullfile('outputs', 'smac_mimetics_in_silico_clinical_trial.mat'),...
    'simulation_inputs', 'results');
end
