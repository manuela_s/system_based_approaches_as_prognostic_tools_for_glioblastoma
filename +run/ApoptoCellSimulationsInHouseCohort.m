function ApoptoCellSimulationsInHouseCohort()
% Run APOPTO-CELL simulations for the GBM in-house cohort, including
% patients from both the 'this_study' and 'MurphyCDDis2013' sub_cohorts.

% Get WB-based APOPTO-CELL simulation inputs
simulation_inputs = get.InHouseApoptoCellSimulationInputs();

% Run simulations
results = cohort_utilities.SimulateCohort(simulation_inputs,...
    'duration', 60,...
    'progressbar_label', 'ApoptoCellSimulationsInHouseCohort'); %#ok<NASGU>

% Save simulation results
save(fullfile('outputs', 'in_house_simulation_results.mat'));
end
